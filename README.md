# Site da Comunidade Debian Minas Gerais

## Debian Day 2023

Este ano o Projeto Debian completa 30 anos de existência e para comemorar,
voluntários(as) organizam atividades em diversas cidades do Brasil e do mundo em
um evento conhecido como [Debian Day](https://wiki.debian.org/DebianDay).

Como Belo Horizonte não poderia ficar de fora desta celebração, o Grupo de
Usuários Debian Minas Gerais (GUD-MG), a comunidade de Sotftware Livre de BH e
Reigão e o Departamento de Ciência da Computação da UFMG realizarão o **Dia do
Debian 2023**.

### Local

* [Sala multiuso](https://www.ufmg.br/espacodoconhecimento/descubra/sala-multiuso)
do espaço do conhecimento da UFMG.

### Data e horário

* 12 de agosto de 2023 (sábado)
* Das 10h às 17h

### Inscrição

A entrada é gratuita e não é necessário fazer inscrição prévia. Basta chegar e
participar.

Queremos receber desde usuários(as) inexperientes que estão iniciando o seu
contato com o Debian até Desenvolvedores(as) oficiais do Projeto. Ou seja,
estão todos(as) convidados(as)!

### Programação

- 10:00h - O projeto Debian quer você! Paulo Henrique de Lima Santana
- 11:00h - Personalizando a Debian para uso em escolas da PBH: a história da Libertas - Fred Guimarães
- 14:00h - Bate-papo sobre os próximos passos para formar uma comunidade de Software Livre em BH - Bruno Braga Fonseca

Obs: se você gostaria de falar sobre algo relacionado ao Debian, entre em
contato com a gente. Sobre temas técnicos ou não técnicos. Por exemplo, pode
ser algo sobre como você usa o Debian no seu trabalho ou no seu computador
pessoal, dicas de uso, etc.

### Organização

 * [Comunidade Debian-MG](https://debian-minas-gerais.gitlab.io/site/)
 * [Comunidade de Software Livre de BH e Região](https://softwarelivre.bhz.br)
 * [DCC - Departamento de Ciência da Computação da UFMG](https://dcc.ufmg.br)

## Contato

[Grupo no telegram](https://t.me/debianmg)

[E-mail](mailto:phls@debian.org)


Copyright © 2022—2023 Comunidade Debian Minas Gerais.
[Código-fonte deste site.](https://gitlab.com/debian-minas-gerais/site)´
